FROM node:lts-alpine

LABEL maintainer="timothy.santiago@lps.co.nz"

WORKDIR /usr/src/app

VOLUME [ "/usr/src/app" ]

RUN npm install -g nodemon

COPY . .
RUN npm install

CMD [ "nodemon", "-x", "node rabbit_slave/rabbit_slave.js || touch rabbit_slave/rabbit_slave.js" ]