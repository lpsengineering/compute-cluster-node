const csvParse = require('csv-parse/lib/sync');
const Obj2csv = require('objects-to-csv');
const fs = require('fs');
const RateReference = require('./rateReference/app');
const _ = require('lodash');
const Moment = require('moment');
const MomentRange = require('moment-range');
const moment = MomentRange.extendMoment(Moment);
const FFLogic = require('./src/ffLogic');
const HertzLogic = require('./src/hertzLogic');
const AceLogic = require('./src/aceLogic');

let DATE_FORMAT = 'MM/DD/YYYY 00:00'; //TODO: Time?

// init moment shorthand
moment.updateLocale('en', {
    monthsShort: [
        'Jan',
        'Feb',
        'Mar',
        'Apr',
        'May',
        'Jun',
        'Jul',
        'Aug',
        'Sep',
        'Oct',
        'Nov',
        'Dec'
    ]
});
moment.locale('en'); //TODO: multi-locale support

function decodeCSVToPojo(input) {
    const records = csvParse(input, {
        columns: true,
        skip_empty_lines: true
    });
    return records;
}

function stripCarCode(carName) {
    return carName.split('_')[0];
}

function dateStringToTimestamp(dateString) {
    var timestamp = moment(dateString, 'DD/MM/YYYY');

    return timestamp;
}

function getSeasonFromShorthand(seasonText) {
    var seasonSplit = seasonText.split('-');
    var month = moment.utc(seasonSplit[0], 'MMM').toISOString();
    var weekNum = parseInt(seasonSplit[1].substr(-1));
    var week = getWeeksInAMonth(month)[weekNum - 1];
    return week;
}

//TODO: move to own moment util class
function getWeeksInAMonth(startDate) {
    var firstDay = moment(startDate).startOf('month');
    var endDay = moment(startDate).endOf('month');

    var monthRange = moment.range(firstDay, endDay);
    var weeks = [];
    for (let day of monthRange.by('days')) {
        if (!_.includes(weeks, day.week())) {
            weeks.push(day.week());
        }
    }

    var calendar = [];
    weeks.forEach(function(week) {
        firstWeekDay = moment()
            .week(week)
            .day(1);
        lastWeekDay = moment()
            .week(week)
            .day(7);
        weekRange = moment.range(firstWeekDay, lastWeekDay);

        calendar.push(weekRange);
    });
    return calendar;
}

function getAceObject(startDate) {
    var date0 = _.find(startDate, { Duration: '0' });
    var date1 = _.find(startDate, { Duration: '1' });
    var date3 = _.find(startDate, { Duration: '3' });
    var date8 = _.find(startDate, { Duration: '8' });
    var date14 = _.find(startDate, { Duration: '14' });
    return {
        channelcarId: date0.channelcarId,
        Startdate: date0.Startdate,
        Region: date0.Region,
        priceDay0: date0.Acceptedprice,
        priceDay1: date1.Acceptedprice,
        priceDay3: date3.Acceptedprice,
        priceDay8: date8.Acceptedprice,
        priceDay14: date14.Acceptedprice
    };
}

module.exports = {
    processCsv: async function(input, source) {
        if (source == 'Firefly') {
            var records = decodeCSVToPojo(input);
            var results = [];
            FFLogic.getAllProducts().forEach(product => {
                var result = records.map(record => {
                    var calculation = FFLogic.calculateWithProduct(
                        record,
                        product
                    );
                    var discount = FFLogic.getDiscountForDuration(
                        calculation.duration,
                        product
                    );
                    return {
                        carCode: stripCarCode(record['CarName']),
                        ProductCode: discount != null ? discount.code : 'N/A',
                        duration:
                            discount != null ? discount.durationCode : 'N/A',
                        seasonStart: moment(record['Startdate'], 'DD-MM-YYYY')
                            .format('MM-DD-YYYY 00:00')
                            .toString(),
                        timestamp: dateStringToTimestamp(record['Startdate']),
                        // seasonEnd: season.end.format(DATE_FORMAT).toString(),
                        output: calculation['calculatedPrice'].value.toString(),
                        calculation: FFLogic.getCalculationText(
                            calculation,
                            product
                        )
                    };
                });
                results = results.concat(result);
            });

            _.remove(results, code => {
                return code.ProductCode == 'N/A';
            });
            results = _.sortBy(results, [
                'ProductCode',
                'timestamp',
                'carCode'
            ]);
            results.forEach(record => delete record.timestamp);
            var debugCsv = await new Obj2csv(results);
            await debugCsv.toDisk('output.csv', {});
            // results.forEach(record => delete record.calculation);
            var csv = await new Obj2csv(results);
            return csv;
        } else if (source == 'Hertz') {
            var records = decodeCSVToPojo(input);
            var results = [];
            var products = HertzLogic.getAllProducts();
            records.forEach(record => {
                var logic = new HertzLogic(record);
                var productResults = products.map(product => {
                    logic.setProduct(product);
                    var calculation = logic.calculateWithProduct(
                        record,
                        product
                    );
                    var discount = logic.getDiscountForDuration(
                        parseInt(record['Duration'])
                    );
                    var res = {
                        carCode: stripCarCode(record['CarName']),
                        ProductCode:
                            discount != null
                                ? discount.discountRatePlan
                                : 'N/A',
                        duration: record['Duration'], //TODO: append duration code to logic csv
                        startDate: record['Startdate'],
                        timestamp: dateStringToTimestamp(
                            moment(
                                record['Startdate'.substring(0, 10)],
                                'YYYY-MM-DD'
                            )
                        ),
                        output: calculation.total.value.toString(),
                        calculation: calculation.calculation
                    };
                    return res;
                });
                
                results = results.concat(productResults);
                
            });
            
            _.remove(results, code => {
                return code.ProductCode == 'NULL';
            });
            
            results = _.sortBy(results, [
                'ProductCode',
                'timestamp',
                'carCode'
            ]);
            results.forEach(record => delete record.timestamp);
            var debugCsv = await new Obj2csv(results);
            await debugCsv.toDisk('output.csv');
            // results.forEach(record => delete record.calculation);
            var csv = await new Obj2csv(results);
            return csv;
        } else if (source == 'Ace AU') {
            var records = decodeCSVToPojo(input);
            var groupedByRegion = _.groupBy(records, 'Region');
            var results = [];
            _.forEach(groupedByRegion, region => {
                var groupedByCar = _.groupBy(region, 'channelcarId');
                _.forEach(groupedByCar, car => {
                    var groupedByStartdate = _.groupBy(car, 'Startdate');
                    _.forEach(groupedByStartdate, startDate => {
                        var aceObj = getAceObject(startDate);
                        var calculation = AceLogic.calculate(aceObj);
                        results.push({
                            timestamp: dateStringToTimestamp(aceObj.Startdate),
                            Rate_prod: 'AWEB',
                            Rate_eff_date: moment(
                                aceObj.Startdate.substring(0, 10),
                                'DD-MM-YYYY'
                            )
                                .format('DD-MM-YYYY')
                                .toString(),
                            Rate_class: aceObj.channelcarId,
                            Rate_day: calculation.aweb.rateDay,
                            Rate_week: calculation.aweb.rateWeek,
                            Rate_month: calculation.aweb.rateMonth,
                            Rate_xday: calculation.aweb.rateXDay,
                            Calculation: AceLogic.getCalculationText(
                                aceObj,
                                calculation.aweb,
                                'AWEB'
                            )
                        });
                        results.push({
                            timestamp: dateStringToTimestamp(aceObj.Startdate),
                            Rate_prod: 'AWEBE',
                            Rate_eff_date: moment(
                                aceObj.Startdate.substring(0, 10),
                                'DD-MM-YYYY'
                            )
                                .format('DD-MM-YYYY')
                                .toString(),
                            Rate_class: aceObj.channelcarId,
                            Rate_day: calculation.awebe.rateDay,
                            Rate_week: calculation.awebe.rateWeek,
                            Rate_month: calculation.awebe.rateMonth,
                            Rate_xday: calculation.awebe.rateXDay,
                            Calculation: AceLogic.getCalculationText(
                                aceObj,
                                calculation.awebe,
                                'AWEBE'
                            )
                        });
                    });
                });
            });
            results = _.sortBy(results, [
                'Rate_prod',
                'timestamp',
                'Rate_class'
            ]);
            results.forEach(record => delete record.timestamp);
            var debugCsv = await new Obj2csv(results);
            await debugCsv.toDisk('output.csv', {});
            // results.forEach(record => delete record.calculation);
            var csv = await new Obj2csv(results);
            return csv;
        }
    }
};
