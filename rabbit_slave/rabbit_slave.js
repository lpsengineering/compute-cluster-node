var open = require('amqplib').connect(process.env.AMQP_URL);
var calculator = require('../calculator');

var q = 'csvcompute';

open.then(function(conn) {
    return conn.createChannel();
  }).then(function(ch) {
    return ch.assertQueue(q).then(function(ok) {
      return ch.consume(q, async function(msg) {
        if (msg !== null) {
          var source = 'Firefly';
          console.log(`headers: ${msg.properties.headers.source}`);
          if (msg.properties.headers.source != null ) {
            source = msg.properties.headers.source
          }
          console.log(`received message id: ${msg.properties.correlationId}\n replyTo: ${msg.properties.replyTo}`);
          var result = await calculator.processCsv(msg.content.toString(), source);
          var resultString = await result.toString();
          ch.ack(msg);
          ch.sendToQueue(msg.properties.replyTo, 
            Buffer.from(resultString), {
            correlationId: msg.properties.correlationId
          });
        }
        return;
      });
    });
  }).catch(console.warn);