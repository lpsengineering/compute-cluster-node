const csvParse = require('csv-parse/lib/sync');
const fs = require('fs');
const path = require('path');

class RateReference {
    constructor(companyName) {
        this.companyName = companyName;
    }

    getReference() {
        var referenceString = fs.readFileSync(
            path.join(__dirname, `${this.companyName}_extras.csv`),
            'utf8'
        );
        var referenceRecords = csvParse(referenceString, {
            columns: true,
            skip_empty_lines: true
        });
        var reference = {};
        referenceRecords.forEach(element => {
            delete element.CompanyName;
            var locationName = element.LocationName;
            delete element.LocationName;
            reference[locationName] = element;
        });
        return reference;
    }

    getDiscountLogic() {
        var logicText = fs.readFileSync(
            path.join(__dirname, `${this.companyName}_logic.csv`),
            'utf8'
        );
        var logicRecords = csvParse(logicText, {
            columns: true,
            skip_empty_lines: true
        });
        return logicRecords;
    }

    getCNFixedValues() {
        var logicText = fs.readFileSync(
            path.join(__dirname, `${this.companyName}_cnfixed.csv`),
            'utf8'
        );
        
        var fixedValues = csvParse(logicText, {
            columns: true,
            skip_empty_lines: true
        });
        return fixedValues;
    }

    getFixedValues() {
        var logicText = fs.readFileSync(
            path.join(__dirname, `${this.companyName}_fixed.csv`),
            'utf8'
        );
        
        var fixedValues = csvParse(logicText, {
            columns: true,
            skip_empty_lines: true
        });
        return fixedValues;
    }

    getHeaderForDuration(duration) {
        duration = parseInt(duration);
        if ([1, 2, 3, 4].includes(duration)) {
            return `${duration}d`;
        } else if (duration >= 5 && duration < 7) {
            return '<7d';
        } else if (duration >= 7 && duration < 14) {
            return '<14d';
        } else if (duration >= 14 && duration < 28) {
            return '<28d';
        } else {
            return '>=28d';
        }
    }
}

module.exports = RateReference;
