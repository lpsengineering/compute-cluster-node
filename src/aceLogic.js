const _ = require('lodash');
const RateReference = require('../rateReference/app');
const _$ = require('currency.js');

var reference = new RateReference('ff').getReference();

class AceLogic {

    static getGST(price) {
        return price.divide(_$(11));
    }

    static calculate(aceObj) {
        var aweb = {
            rateDay: _$(aceObj.priceDay1).subtract(AceLogic.getGST(_$(aceObj.priceDay1))).value,
            rateDayDiscount: AceLogic.getGST(_$(aceObj.priceDay1)).value,
            rateWeek: _$(aceObj.priceDay3).subtract(AceLogic.getGST(_$(aceObj.priceDay3))).value,
            rateWeekDiscount: AceLogic.getGST(_$(aceObj.priceDay3)).value,
            rateMonth: _$(aceObj.priceDay8).subtract(AceLogic.getGST(_$(aceObj.priceDay8))).value,
            rateMonthDiscount: AceLogic.getGST(_$(aceObj.priceDay8)).value,
            rateXDay: _$(aceObj.priceDay14).subtract(AceLogic.getGST(_$(aceObj.priceDay14))).value,
            rateXDayDiscount: AceLogic.getGST(_$(aceObj.priceDay14)).value,
        }
        var awebe = {
            rateDay: _$(aceObj.priceDay0).subtract(AceLogic.getGST(_$(aceObj.priceDay0))).value,
            rateDayDiscount: AceLogic.getGST(_$(aceObj.priceDay0)).value,
            rateWeek: _$(aceObj.priceDay3).subtract(AceLogic.getGST(_$(aceObj.priceDay3))).value,
            rateWeekDiscount: AceLogic.getGST(_$(aceObj.priceDay3)).value,
            rateMonth: _$(aceObj.priceDay8).subtract(AceLogic.getGST(_$(aceObj.priceDay8))).value,
            rateMonthDiscount: AceLogic.getGST(_$(aceObj.priceDay8)).value,
            rateXDay: _$(aceObj.priceDay14).subtract(AceLogic.getGST(_$(aceObj.priceDay14))).value,
            rateXDayDiscount: AceLogic.getGST(_$(aceObj.priceDay14)).value,
        }
        return { aweb, awebe };
    }

    static getCalculationText(aceObj, result, rateProd) {
        var buffer = '';

        buffer += `Region: ${aceObj.Region}`;
        buffer += `\nGST: 10%`;
        buffer += `\n\nDay Rate:`;
        buffer += `\n   - Accepted Price: ${(rateProd) == 'AWEB' ? aceObj.priceDay1 : aceObj.priceDay0}`;
        buffer += `\n   - GST Deduction (10%): ${result.rateDayDiscount}`;
        buffer += `\n   - Calculated Rate: ${result.rateDay}`;
        buffer += `\n\nWeek Rate:`;
        buffer += `\n   - Accepted Price: ${aceObj.priceDay3}`;
        buffer += `\n   - GST Deduction (10%): ${result.rateWeekDiscount}`;
        buffer += `\n   - Calculated Rate: ${result.rateWeek}`;
        buffer += `\n\nMonth Rate:`;
        buffer += `\n   - Accepted Price: ${aceObj.priceDay8}`;
        buffer += `\n   - GST Deduction (10%): ${result.rateMonthDiscount}`;
        buffer += `\n   - Calculated Rate: ${result.rateMonth}`;
        buffer += `\n\nX Day Rate:`;
        buffer += `\n   - Accepted Price: ${aceObj.priceDay14}`;
        buffer += `\n   - GST Deduction (10%): ${result.rateXDayDiscount}`;
        buffer += `\n   - Calculated Rate: ${result.rateXDay}`;
        
        return buffer;
    }
}

module.exports = AceLogic