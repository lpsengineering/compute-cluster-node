const _ = require('lodash');
const RateReference = require('../rateReference/app');
const _$ = require('currency.js');

var reference = new RateReference('ff').getReference();

class FFLogic {

    static getDiscountForDuration(duration, product) {
        if (product == 'NPP_NZ') {
            switch(true) {
                case (duration == 1):
                    return { amount: 0.0, code: 'F01NNZ', durationCode: 'DY' };
                case (duration == 2):
                    return { amount: 0.0, code: 'F02NNZ', durationCode: 'DY'};
                case (duration == 3):
                    return { amount: 0.0, code: 'F03NNZ', durationCode: 'DY' };
                case (duration == 4):
                    return { amount: 0.0, code: 'F04NNZ', durationCode: 'DY' };
                case (duration < 7):
                    return { amount: 0.0, code: 'F05NNZ', durationCode: 'DY' };
                case (duration < 13):
                    return { amount: 0.0, code: 'F07NNZ', durationCode: 'WK' };
                case (duration < 28):
                    return { amount: 0.0, code: 'F14NNZ', durationCode: 'WK'};
                default:
                    return { amount: 0.0, code: 'F28NNZ', durationCode: 'MO'};
            }
        } else if (product == 'PP_Nett_Domestic') {
            switch(true) {
                case (duration == 1):
                    return { amount: 0.20, code: 'ZRU1', durationCode: 'DY' };
                case (duration == 2):
                    return { amount: 0.20, code: 'ZRU2', durationCode: 'DY' };
                case (duration == 3):
                    return { amount: 0.20, code: 'ZRU3', durationCode: 'DY' };
                case (duration == 4):
                    return { amount: 0.20, code: 'ZRU4', durationCode: 'DY' };
                case (duration < 7):
                    return { amount: 0.20, code: 'ZRU5', durationCode: 'DY' };
                case (duration < 13):
                    return { amount: 0.20, code: 'ZRU6', durationCode: 'WK' };
                case (duration < 28):
                    return { amount: 0.20, code: 'ZRU7', durationCode: 'WK' };
                default:
                    return null;
            }
        } else if (product == 'PP_Nett_Inbound') {
            switch(true) {
                case (duration == 1):
                    return { amount: 0.20, code: 'ZRV1', durationCode: 'DY' };
                case (duration == 2):
                    return { amount: 0.20, code: 'ZRV2', durationCode: 'DY' };
                case (duration == 3):
                    return { amount: 0.20, code: 'ZRV3', durationCode: 'DY' };
                case (duration == 4):
                    return { amount: 0.20, code: 'ZRV4', durationCode: 'DY' };
                case (duration < 7):
                    return { amount: 0.20, code: 'ZRV5', durationCode: 'DY' };
                case (duration < 13):
                    return { amount: 0.20, code: 'ZRV6', durationCode: 'WK' };
                case (duration < 28):
                    return { amount: 0.20, code: 'ZRV7', durationCode: 'WK' };
                default:
                    return null;
            }
        }
    }

    static deductDiscountToResult(result, product) {
        var runningPrice = result.calculatedPrice;
        var discount = FFLogic.getDiscountForDuration(result.duration, product);
        if (discount != null) {
            var deduction = runningPrice.multiply(_$(discount.amount));
            runningPrice = runningPrice.subtract(deduction);
            result.discountedPrice = runningPrice;
            result.calculatedPrice = runningPrice;
            result.deductedDiscount = deduction;
            result.discountAmount = discount.amount;
        }
    }
    
    static addBackExtrasToResult(result) {
        var runningPrice = result.calculatedPrice;
        runningPrice = runningPrice.add(result.gstDeduction);
        runningPrice = runningPrice.add(result.locDeduction);
        result.afterAddExtra = runningPrice;
        result.calculatedPrice = runningPrice;
    }

    static getNonPrepaidRate(record) {
        var region = record['Region'];
        var price = _$(record['Acceptedprice']);
        var duration = _$(record['Duration']);
        var referenceRegion = reference[region];
        var gst = price.multiply(3).divide(23);
        var loc = _$(referenceRegion.APLOC);
        var gstDeduction = gst;
        var locDeduction = loc.divide(duration)
        var gstPrice = price.subtract(gstDeduction);
        var locPrice = gstPrice.subtract(locDeduction);
        var calculatedPrice = locPrice;

        var result = {
            region: region,
            reference: referenceRegion,
            price: price,
            duration: duration,
            gstPrice: gstPrice,
            locPrice: locPrice,
            gstDeduction: gstDeduction,
            locDeduction: locDeduction,
            calculatedPrice: calculatedPrice,
            startDate: record['Startdate']
        }
        return result;
    }

    static calculateWithProduct(record, product) {
        var result = {}
        if (product == 'NPP_NZ') {
            result = FFLogic.getNonPrepaidRate(record);
        } else if (product == 'PP_Nett_Domestic' || product == 'PP_Nett_Inbound') {
            result = FFLogic.getNonPrepaidRate(record);
            FFLogic.addBackExtrasToResult(result);
            FFLogic.deductDiscountToResult(result, product);
        }
        
        //check duration multiplier
        var discount = FFLogic.getDiscountForDuration(result.duration, product);
        result.discount = discount;
        if (discount != null) {
            switch (discount.durationCode) {
                case "WK":
                    result.preMultiplicationPrice = result.calculatedPrice;
                    result.calculatedPrice = result.calculatedPrice.multiply(7);
                    break;
                case "MO":
                    result.preMultiplicationPrice = result.calculatedPrice;
                    result.calculatedPrice = result.calculatedPrice.multiply(28);
                    break;
            }
        }
        return result;
    }

    static getAllProducts() {
        return ['NPP_NZ', 'PP_Nett_Domestic', 'PP_Nett_Inbound'];
    }

    static getCalculationText(record,product) {
        var buffer = '';
        buffer += `Region: ${record.region}`;
        buffer += `\nGST: ${record.reference.GST}%`;
        buffer += `\nLOC: ${record.reference.APLOC}`;
        buffer += `\nVRR: ${record.reference.VRR}`;
        buffer += `\nAdmin: ${record.reference.Admin}`;
        buffer += `\nDiscount: ${_$(record.discountAmount).multiply(_$(100)).value}%`
        buffer += `\nAccepted price: ${record.price}`;
        buffer += `\nDuration: ${record.duration}`;
        buffer += `\nGST Deduction: ${record.price} - ${record.gstDeduction} = ${record.gstPrice}`;
        buffer += `\nLOC Deduction: ${record.gstPrice} - ${record.locDeduction} = ${record.locPrice}`;
        
        if (product != 'NPP_NZ') {
            buffer += `\nAdded back extras: \n  GST: ${record.gstDeduction}\n  LOC: ${record.locDeduction}\n  = ${record.afterAddExtra}`;
            buffer += `\nDeducted discount: \n  - ${record.deductedDiscount} = ${record.discountedPrice}`;
        }

        if (record.discount != null && product == 'NPP_NZ') {
            switch (record.discount.durationCode) {
                case "WK":
                    buffer += `\nWeekly rental: ${record.preMultiplicationPrice} x 7 = ${record.calculatedPrice}`;
                    break;
                case "MO":
                    buffer += `\nMonthly rental: ${record.preMultiplicationPrice} x 28 = ${record.calculatedPrice}`;
                    break;
            }
        }
        
        buffer += `\nCalculated Total: ${record.calculatedPrice}`;
        return buffer;
    }
}

module.exports = FFLogic