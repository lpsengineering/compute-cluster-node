const _ = require('lodash');
const RateReference = require('../rateReference/app');
const _$ = require('currency.js');

var reference = new RateReference('hertz');

class HertzLogic {
    constructor(record) {
        this.record = record;
    }

    setProduct(product) {
        this.product = product;
    }

    static getAllProducts() {
        var logicTable = reference.getDiscountLogic();
        return logicTable.map(logic => logic['Product Desc']);
    }

    getDiscountForDuration(duration) {
        var header = reference.getHeaderForDuration(duration);
        var logicTable = reference.getDiscountLogic();
        var logic = _.find(logicTable, { 'Product Desc': this.product });

        return {
            discount: logic[header],
            discountRatePlan: logic[`${header}_RP`],
            company: logic['Company'],
            sourceMarket: logic['Source Market'],
            calculation: logic['Calculation'],
            planType: logic['PlanType']
        };
    }

    getDiscountForDurationProduct(duration, product) {
        var header = reference.getHeaderForDuration(duration);
        var logicTable = reference.getDiscountLogic();
        var logic = _.find(logicTable, { 'Product Desc': product });

        // console.log(header);
        return {
            discount: logic[header],
            discountRatePlan: logic[`${header}_RP`],
            company: logic['Company'],
            sourceMarket: logic['Source Market'],
            calculation: logic['Calculation'],
            planType: logic['PlanType']
        };
    }

    getDiscount(record, price) {
        var discountObj = this.getDiscountForDuration(
            record['Duration'],
            this.product
        );
        return _$(price.value * (discountObj.discount * 0.01));
    }

    deductDiscountToResult(result, product) {
        var runningPrice = result.calculatedPrice;
        var discount = this.getDiscountForDuration(result.duration, product);
        if (discount != null) {
            var deduction = runningPrice.multiply(_$(discount.amount));
            runningPrice = runningPrice.subtract(deduction);
            result.discountedPrice = runningPrice;
            result.calculatedPrice = runningPrice;
            result.deductedDiscount = deduction;
        }
    }

    getExtras(record, price) {
        var region = record['Region'];
        var duration = _$(record['Duration']);
        var referenceRegion = reference.getReference()[region];
        var gst = price.multiply(3).divide(23);
        var loc = _$(referenceRegion.APLOC).divide(duration);

        return loc.add(gst);
    }

    ppUSLogic(duration) {
        //TODO REFER FROM CSV
        
        //TODO REFER FROM CSV
        duration = parseInt(duration);
        if ([1, 2, 3].includes(duration)) {
            return 0.02;
        } else if (duration == 4){
            return 0.25;
        } else if (duration >= 5 && duration <= 6) {
            return 0.18;
        } else if (duration >= 7 && duration < 14) {
            return 0.11;
        } else if (duration >= 14 && duration < 28) {
            return 0.2;
        } else if(duration >= 28){
            return 0.05;
        }

        // switch (true) {
        //     case duration == 1:
        //         return 0.02;
        //     case duration == 2:
        //         return 0.02;
        //     case duration == 3:
        //         return 0.02;
        //     case duration == 4:
        //         return 0.25;
        //     case duration == 7:
        //         return 0.18;
        //     case duration == 14:
        //         return 0.11;
        //     case duration == 28:
        //         return 0.2;
        //     case duration >= 28:
        //         return 0.05;
        //     default:
        //         break;
        // }
    }

    cfvDiscountLogic(duration, plan, record) {

        var carId = record.channelcarId;
        var cfvTable = reference.getCNFixedValues();
        var logic = _.find(cfvTable, {'Group' : carId});
        // console.log(logic[plan]);
        return logic[plan];

        // switch (true) {
        //     case duration == 1:
        //         return { amount: 38 };
        //     case duration == 2:
        //         return { amount: 38 };
        //     case duration == 3:
        //         return { amount: 38 };
        //     case duration == 4:
        //         return { amount: 38 };
        //     case duration == 6:
        //         return { amount: 38 };
        //     case duration == 7:
        //         return { amount: 38 };
        //     case duration == 14:
        //         return { amount: 27.14 };
        //     case duration == 28:
        //         return { amount: 13.57 };
        //     default:
        //         break;
        // }
    }
    
    fvAddLogic(car) {
        var fvTable = reference.getFixedValues();
        var logic = _.find(fvTable, { 'Group': car });
        return logic.Max;
    }

    getGST(price) {
        return _$(price.multiply(3).divide(23));
    }

    performOperationForCode(code, record, result) {
        var action = code.substring(0, 1);
        var operation = code.substring(1);

        var operationResult;
        switch (operation) {
            case 'NPP_NZ':
                // Remove GST and LOC
                // LOC deduction is also based on duration
                var extraToSubtract = this.getExtras(
                    record,
                    _$(record['Acceptedprice'])
                );
                operationResult = _$(record['Acceptedprice']).subtract(
                    extraToSubtract
                );

                var discount = this.getDiscount(
                    record,
                    result.total,
                    operation
                );
                result.calculation += '\nNon-Prepaid: ' + discount;

                break;
            case 'NPP_US':
                var extraToSubtract = this.getExtras(
                    record,
                    _$(record['Acceptedprice'])
                );
                var discount = this.getDiscountForDurationProduct(
                    result.duration,
                    operation
                );
                
                var priceBeforePPDiscount = _$(
                    record['Acceptedprice']
                ).subtract(extraToSubtract);

                operationResult = _$(record['Acceptedprice']).subtract(
                    extraToSubtract
                );
                discount.discount = discount.discount / 10;
                operationResult = operationResult.subtract(
                    operationResult.multiply(discount.discount)
                );
                result.calculation +=
                    '\nNPP Discount: ' + discount.discount + '%';
                result.calculation +=
                    '\nNPP Deduction: ' +
                    priceBeforePPDiscount +
                    ' - ' +
                    operationResult.multiply(discount.discount) +
                    ' = ' +
                    operationResult;
            case 'PP_US':
                var extraToSubtract = this.getExtras(
                    record,
                    _$(record['Acceptedprice'])
                );

                var duration = record['Duration'];
                var discount = _$(this.ppUSLogic(duration));

                var priceBeforePPDiscount = _$(
                    record['Acceptedprice']
                ).subtract(extraToSubtract);

                operationResult = _$(record['Acceptedprice']).subtract(
                    extraToSubtract
                );

                operationResult = operationResult.subtract(
                    operationResult.multiply(discount)
                );

                result.calculation += '\nNPP Discount: ' + discount + '%';
                result.calculation +=
                    '\nNPP Deduction: ' +
                    priceBeforePPDiscount +
                    ' - ' +
                    operationResult.multiply(discount) +
                    ' = ' +
                    operationResult;

                break;
            case 'PP_UK':
                break;
            case 'NPP_AU':
                // Remove GST and LOC
                // LOC deduction is also based on duration
                // Check AU GST

                var extraToSubtract = this.getExtras(
                    record,
                    _$(record['Acceptedprice'])
                );
                operationResult = _$(record['Acceptedprice']).subtract(
                    extraToSubtract
                );
                break;
            case 'NPP_ASIA':
                var extraToSubtract = this.getExtras(
                    record,
                    _$(record['Acceptedprice'])
                );
                var discount = this.getDiscountForDurationProduct(
                    result.duration,
                    operation
                );
                
                var priceBeforePPDiscount = _$(
                    record['Acceptedprice']
                ).subtract(extraToSubtract);

                operationResult = _$(record['Acceptedprice']).subtract(
                    extraToSubtract
                );
                discount.discount = discount.discount / 10;
                operationResult = operationResult.subtract(
                    operationResult.multiply(discount.discount)
                );
                result.calculation +=
                    '\nNPP Discount: ' + discount.discount + '%';
                result.calculation +=
                    '\nNPP Deduction: ' +
                    priceBeforePPDiscount +
                    ' - ' +
                    operationResult.multiply(discount.discount) +
                    ' = ' +
                    operationResult;
                break;
            case 'NPP_EMEA':
                var extraToSubtract = this.getExtras(
                    record,
                    _$(record['Acceptedprice'])
                );
                var discount = this.getDiscountForDurationProduct(
                    result.duration,
                    operation
                );
                
                var priceBeforePPDiscount = _$(
                    record['Acceptedprice']
                ).subtract(extraToSubtract);

                operationResult = _$(record['Acceptedprice']).subtract(
                    extraToSubtract
                );
                discount.discount = discount.discount / 10;
                operationResult = operationResult.subtract(
                    operationResult.multiply(discount.discount)
                );
                result.calculation +=
                    '\nNPP Discount: ' + discount.discount + '%';
                result.calculation +=
                    '\nNPP Deduction: ' +
                    priceBeforePPDiscount +
                    ' - ' +
                    operationResult.multiply(discount.discount) +
                    ' = ' +
                    operationResult;
                break;
            case 'NPP_CN':
                var extraToSubtract = this.getExtras(
                    record,
                    _$(record['Acceptedprice'])
                );
                var discount = this.getDiscountForDurationProduct(
                    result.duration,
                    operation
                );
                var priceBeforePPDiscount = _$(
                    record['Acceptedprice']
                ).subtract(extraToSubtract);

                operationResult = _$(record['Acceptedprice']).subtract(
                    extraToSubtract
                );
                discount.discount = discount.discount / 10;
                operationResult = operationResult.subtract(
                    operationResult.multiply(discount.discount)
                );
                result.calculation +=
                    '\nNPP Discount: ' + discount.discount + '%';
                result.calculation +=
                    '\nNPP Deduction: ' +
                    priceBeforePPDiscount +
                    ' - ' +
                    operationResult.multiply(discount.discount) +
                    ' = ' +
                    operationResult;
                break;
            case 'DC':
                // Discounts
                operationResult = _$(this.getDiscount(record, result.total));
                var deductedDiscount = _$(record['Acceptedprice']).subtract(
                    operationResult
                );

                var discount = this.getDiscountForDuration(
                    result.duration,
                    this.product
                );

                discount.discount = discount.discount / 10;
                discount.discount = result.total.multiply(discount.discount);
                operationResult = discount.discount;
                // if(action == 'A'){
                //     var final = result.total.add(discount.discount);
                //     result.calculation +=
                //     `\nAdded discount:\n ${result.total} + ` +
                //     discount.discount +
                //     ' = ' +
                //     final;
                // }else{
                    var final = result.total.subtract(discount.discount);
                    result.calculation +=
                    `\nDeducted discount:\n ${result.total} - ` +
                    discount.discount +
                    ' = ' +
                    final;
                // }
                
                break;
            case 'CFV':
                // CN Fixed Value
                var duration = record['Duration'];
                
                var discount = this.getDiscountForDuration(
                    result.duration,
                    operation
                );
                operationResult = this.cfvDiscountLogic(duration, discount.discountRatePlan, record);
                result.calculation += `\nAdded Fixed Value: ${
                    result.total
                } + ${operationResult} = ${result.total.add(
                    _$(operationResult)
                )}`;
                break;
            case 'FV':
                var carId = record.channelcarId;
                var fvAmount = _$(this.fvAddLogic(carId));
                operationResult = fvAmount;
                result.calculation += '\nAdd Fixed Value by Car Type: +' + ' ' + fvAmount
                break;
            case 'EX':
                //  Add back Extras
                operationResult = this.getExtras(
                    record,
                    _$(record['Acceptedprice'])
                );

                var locDeduction = result.loc.divide(result.duration);
                var runPrice = result.total.add(locDeduction).multiply(1.15);
                var currPrice = result.total.add(locDeduction);
                var gst = runPrice.subtract(currPrice);
                operationResult = locDeduction.add(gst);
                result.calculation +=
                    '\nAdded Back Extras: \n  GST: ' +
                    gst +
                    '\n  LOC: ' +
                    locDeduction +
                    '\n  = ' +
                    runPrice;
                break;
            case 'DUR':
                var duration = this.getCodeForDuration(
                    parseInt(record['Duration'])
                );
                operationResult = duration.multiplier;
                result.durationCode = duration.code;
        }

        if (operationResult != null) {
            if (action == 'A') {
                // Addition
                result.total = result.total.add(operationResult);
            } else if (action == 'D') {
                result.total = result.total.subtract(operationResult);
            } else if (action == 'M') {
                var multiplied = result.total.multiply(operationResult);
                result.calculation += `\nMultiplied by Duration (${
                    result.durationCode
                }): ${
                    result.total
                } x ${operationResult} = ${multiplied.toString()}`;
                result.total = multiplied;
            }
            return result;
        }
    }

    getCodeForDuration(duration) {
        if (duration < 7) {
            return { code: 'DY', multiplier: 1 };
        } else if (duration < 28) {
            return { code: 'WK', multiplier: 7 };
        }
        return { code: 'MO', multiplier: 28 };
    }

    calculateWithProduct(record, product) {
        // var fixedValues = reference.getFixedValues();
        // console.log(fixedValues);

        var result = {};

        result.duration = record['Duration'];
        result.discount = this.getDiscountForDuration(result.duration);
        result.total = _$(0);
        var region = record['Region'];
        var referenceRegion = reference[region];
        var ref = reference.getReference();
        var referenceRecord = ref[region];
        var price = _$(record['Acceptedprice']);

        var gst = price.multiply(3).divide(23);
        var gstPrice = _$(price.subtract(gst));
        result.loc = _$(referenceRecord.APLOC);
        result.locDeduction = result.loc.divide(result.duration);
        result.locPrice = gstPrice.subtract(result.locDeduction);
        var runningPrice = gst.add(result.locDeduction);
        runningPrice = result.locPrice.add(gst).add(result.locDeduction);

        var deductedDiscount = runningPrice.multiply(_$(result.discount));
        var discount = result.discount.discount / 10;

        var buffer = '';
        buffer += `Region: ${record['Region']}`;
        buffer += `\nGST: ${referenceRecord.GST}%`;
        buffer += `\nLOC: ${result.loc}`;
        buffer += `\nVRR: ${referenceRecord.VRR}`;
        buffer += `\nAdmin: ${referenceRecord.Admin}`;
        // buffer += `\nNPP Rate: ${result.discount.discountRatePlan}`;
        // buffer += `\nNPP Calculation: ${result.discount.calculation}`;
        buffer += `\nDiscount: ${_$(discount)}%`;
        buffer += `\nAccepted price: ${record['Acceptedprice']}`;
        buffer += `\nDuration: ${record['Duration']}`;
        buffer += `\nGST Deduction: ${
            record['Acceptedprice']
        } - ${gst} = ${gstPrice}`;
        buffer += `\nLOC Deduction: ${gstPrice} - ${result.locDeduction} = ${
            result.locPrice
        }`;

        result.calculation = buffer;

        var calcChunks = result.discount.calculation.split('-');
        // var newVal;
        calcChunks.forEach(chunk => {
            this.performOperationForCode(chunk, record, result);
        });

        result.calculation += '\nCalculated Total: ' + result.total;
        result.durationCode = this.getCodeForDuration(
            parseInt(result.duration)
        );

        return result;

        if (product == 'NPP_NZ') {
            result = FFLogic.getNonPrepaidRate(record);
        } else if (
            product == 'PP_Nett_Domestic' ||
            product == 'PP_Nett_Inbound'
        ) {
            result = FFLogic.getNonPrepaidRate(record);
            FFLogic.addBackExtrasToResult(result);
            FFLogic.deductDiscountToResult(result, product);
        }

        //check duration multiplier
        var discount = FFLogic.getDiscountForDuration(result.duration, product);
        result.discount = discount;
        if (discount != null) {
            switch (discount.durationCode) {
                case 'WK':
                    result.preMultiplicationPrice = result.calculatedPrice;
                    result.calculatedPrice = result.calculatedPrice.multiply(7);
                    break;
                case 'MO':
                    result.preMultiplicationPrice = result.calculatedPrice;
                    result.calculatedPrice = result.calculatedPrice.multiply(
                        28
                    );
                    break;
            }
        }
        return result;
    }

    getCalculationText(record, product) {
        // console.log(record);
        // console.log(product);

        var price = _$(product['Acceptedprice']);
        // console.log(price.value);
        // console.log()

        var gst = price.multiply(3).divide(23);
        var gstPrice = _$(price.subtract(gst));

        var buffer = '';
        buffer += `Region: ${product.Region}`;
        // buffer += `\nGST: ${record.reference.GST}%`;
        // buffer += `\nLOC: ${record.reference.APLOC}`;
        // buffer += `\nVRR: ${record.reference.VRR}`;
        // buffer += `\nAdmin: ${record.reference.Admin}`;
        // buffer += `\nDiscount: ${_$(record.discountAmount).multiply(_$(100)).value}%`
        // buffer += `\nAccepted price: ${record.price}`;
        // buffer += `\nDuration: ${record.duration}`;
        // buffer += `\nGST Deduction: ${record.price} - ${record.gstDeduction} = ${record.gstPrice}`;
        // buffer += `\nLOC Deduction: ${record.gstPrice} - ${record.locDeduction} = ${record.locPrice}`;

        // if (product != 'NPP_NZ') {
        //     buffer += `\nAdded back extras: \n  GST: ${record.gstDeduction}\n  LOC: ${record.locDeduction}\n  = ${record.afterAddExtra}`;
        //     buffer += `\nDeducted discount: \n  - ${record.deductedDiscount} = ${record.discountedPrice}`;
        // }

        // if (record.discount != null && product == 'NPP_NZ') {
        //     switch (record.discount.durationCode) {
        //         case "WK":
        //             buffer += `\nWeekly rental: ${record.preMultiplicationPrice} x 7 = ${record.calculatedPrice}`;
        //         break;
        //         case "MO":
        //             buffer += `\nMonthly rental: ${record.preMultiplicationPrice} x 28 = ${record.calculatedPrice}`;
        //         break;
        //     }
        // }

        // buffer += `\nCalculated Total: ${record.calculatedPrice}`;
        return buffer;
    }
}

module.exports = HertzLogic;
