var calc = require('./calculator');
var fs = require('fs');
var obj2csv = require('objects-to-csv');

async function testFF() {
    var inputcsv = fs.readFileSync('./input.csv', 'utf8');
    var result = await calc.processCsv(inputcsv, 'Firefly');
    // console.log(result);
    result.toDisk('raw_output.csv');
}

async function testHertz() {
    var inputcsv = fs.readFileSync('./HZ/hertz_input.csv', 'utf8');
    var result = await calc.processCsv(inputcsv, 'Hertz');
    // console.log(result);
}

async function testAceAU() {
    var inputcsv = fs.readFileSync('./aceau_input.csv', 'utf8');
    var result = await calc.processCsv(inputcsv, 'Ace AU');
    // console.log(result);
}

testHertz();
